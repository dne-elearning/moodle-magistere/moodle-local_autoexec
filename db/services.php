<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * local_taskmanager external functions and service definitions.
 *
 * @package    local_taskmanager
 * @copyright  2021 TCS
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */



$services = array(
    'local_autoexec_service' => array(
        'functions' => array(
            'local_autoexec_get_alltasks'
        ),
        'requiredcapability' => '',
        'restrictedusers' => 0,
        'enabled' => 1,
        'shortname' =>  'local_autoexec_service',
        'downloadfiles' => 0,
        'uploadfiles'  => 0,
    )
);

$functions = array(
    'local_autoexec_get_alltasks' => array(
        'classname' => 'local_autoexec_external',
        'methodname' => 'get_alltasks',
        'classpath' => 'local/autoexec/externallib.php',
        'description' => ' Get the tasks of all instances',
        'type' => 'read',
        'capavilities' => '',
        'ajax' => true,
    ),
    'local_autoexec_update_task' => array(
        'classname' => 'local_autoexec_external',
        'methodname' => 'update_task',
        'classpath' => 'local/autoexec/externallib.php',
        'description' => 'Update a task',
        'type' => 'write',
        'capavilities' => '',
        'ajax' => true,
    ),
);
