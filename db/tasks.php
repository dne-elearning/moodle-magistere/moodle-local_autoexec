<?php

$tasks = array(
    array(
        'classname' => 'local_autoexec\task\check_upgrade_task',
        'blocking' => 0,
        'minute' => '*',
        'hour' => '*',
        'day' => '*',
        'dayofweek' => '*',
        'month' => '*',
        'disabled' => 1
    )
);