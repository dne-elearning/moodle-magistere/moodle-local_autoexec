<?php

/**
 * Language file
 *
 * @package    local_autoexec
 * @copyright  2021 TCS
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

$string['instance']         = 'Instance';
$string['taskname']         = 'Task name';
$string['taskclass']        = 'Class';
$string['taskpath']         = 'Task file path';
$string['taskfileexist']    = 'Task file exist';
$string['version']          = 'Version';
$string['ticket']           = 'Ticket';
$string['execute']          = 'Execute';
$string['delay']            = 'Delay';
$string['dependencies']     = 'Dependancies';
$string['executeruntime']   = 'Runtime';
$string['executestarttime'] = 'Starttime';
$string['executeendtime']   = 'Endtime';
$string['executelogs']      = 'Logs';
$string['executestatus']    = 'Status';


$string['exec_auto'] = 'Auto';
$string['exec_manual'] = 'Manual';
$string['exec_0']    = 'Unknown';
$string['exec_2']    = 'Success';
$string['exec_3']    = 'Running';
$string['exec_4']    = 'Fail';
$string['exec_5']    = 'Error';
$string['exec_6']    = 'Todo';
$string['exec_7']    = 'Queued';
$string['exec_8']    = 'Manual';
$string['exec_9']    = 'Programming';

$string['exist'] = 'Exist';
$string['noexist'] = 'Unknow';

