/* jshint ignore:start */
define(['jquery','jqueryui', 'core/ajax', 'local_autoexec/jtable'], function($, ui, ajax) {

    var str;
    function init(lstr) {
	
		str = lstr;
  		showjtable();
        
        $("#managetable").on('click', '.todobutton', function (e){
            var params = e.currentTarget.id.split('_');
            
            if (params.length != 3){console.log('.todobutton clicked'); return;}
            
            var data = new Object();
            data.action = params[0];
            data.instance = params[1];
            data.id = params[2];

            var promises = ajax.call([
                { methodname: 'local_autoexec_update_task', args: data },
            ])
            promises[0].done((response) => {
                JSON.parse(response.data);
                
                $('#managetable').jtable('reload');
            });
            
        });
    }
    
	
	function showjtable() {
        
		$("#managetable").jtable({
            title: "Titre",
            paging: true,
            pageSize: 20,
            pageSizes: [20,30,50,100],
            selecting: false,
            multiselect: false,
            selectingCheckboxes: false,
            sorting: true,
            defaultSorting: "name ASC",
            jqueryuiTheme: true,
            defaultDateFormat: "dd/mm/yy",
            gotoPageArea: "none",
            selectOnRowClick: false,
            actions: {
                listAction: function (postData, jtParams) {
                    return $.Deferred(function ($dfd) {
                        var postdata = formdata();
						postdata.si = (jtParams.jtStartIndex==undefined?0:jtParams.jtStartIndex);
						postdata.ps = (jtParams.jtPageSize==undefined?0:jtParams.jtPageSize);
			            postdata.so = (jtParams.jtSorting==undefined?'':jtParams.jtSorting);

						var promises = ajax.call([
				            { methodname: 'local_autoexec_get_alltasks', args: postdata },
				        ])
				        promises[0].done((response) => {
			    		    $dfd.resolve(JSON.parse(response.data));
				        });
                    });
                }
            },
            fields:{
                id: {
                    type: 'hidden',
                    key: true,
                },
                instance: {
                    title: str.manageall_header_instance,
                    width: "10%",
                },
                taskname: {
                    title: str.manageall_header_taskname,
                    width: "10%",
                },
                taskclass: {
                    title: str.manageall_header_taskclass,
                    width: "10%",
                },
                taskpath: {
                    title: str.manageall_header_taskpath,
                    width: "10%",
                    sorting: false,
                },
                taskfileexist: {
                    title: str.manageall_header_taskfileexist,
                    width: "10%",
					type: 'text'
                },
                version: {
                    title: str.manageall_header_version,
                    width: "10%"
                },
                ticket: {
                    title: str.manageall_header_ticket,
                    width: "10%"
                },
                execute: {
                    title: str.manageall_header_execute,
                  	width: "10%"
                },
                delay: {
                    title: str.manageall_header_delay,
             	 	width: "10%"
                },
                dependencies: {
                    title: str.manageall_header_dependencies,
                    width: "10%"
                },
                executeruntime: {
                    title: str.manageall_header_executeruntime,
                    width: "10%",
					
                },
                executestarttime: {
                    title: str.manageall_header_executestarttime,
                    width: "10%",
					
                },
                executeendtime: {
                    title: str.manageall_header_executeendtime,
                    width: "10%",
					
                },
                executelogs: {
                    title: str.manageall_header_executelogs,
                    width: "10%",
					//listClass: (data) => {console.log(data)},
                    sorting: false,
                },
                executestatus: {
                    title: str.manageall_header_executestatus,
                    width: "10%",
					//listClass: "statusSuccess",
					display: (data) =>{ 
						//console.log(data);
						classe = "status"+ data.record.executestatus ; 
						
						return '<div class='+classe+'>'+data.record.executestatus+'</div>';
						
					}
		
            	}, 
                actions:{
                    title : 'actions',
                    width: "10%",
                }
			}
        });
		
        $('#managetable').jtable('load');
    }
    
    function formdata() {
    	var data = new Object();
    	return data;
    }
	

	return {
    	init : function(str) {
            init(str);
        }
    };
});
