<?php

require_once(dirname(dirname(dirname(__FILE__))).'/config.php');
require_once($CFG->dirroot.'/local/autoexec/autoexec.php');

require_login();

if ($USER->id != 2) {
    print_error('Acces denied');
}

$PAGE->requires->jquery();
$PAGE->requires->jquery_plugin('ui');
$PAGE->requires->jquery_plugin('ui-css');

$PAGE->set_pagelayout('frontal_notconnected');
$PAGE->set_context(context_system::instance());

$pageparams = array();
$PAGE->set_url('/local/autoexec/manage.php', $pageparams);

$taskclass = optional_param('taskclass', false, PARAM_ALPHANUMEXT);
$changestatus = optional_param('changestatus', false, PARAM_INT);

if ($taskclass !== false && $changestatus !== false) {
    
    $task = $DB->get_record(autoexec::TABLE_AUTOEXEC, array('taskclass'=>$taskclass));
    
    if ($task !== false) {
        if ($changestatus == autoexec::EXEC_TODO) {
            autoexec::change_task_status($taskclass, $changestatus);
            autoexec::reset_task($taskclass);
            autoexec::schedule_task($taskclass);
        } else if ($changestatus == autoexec::EXEC_MANUAL) {
            autoexec::change_task_status($taskclass, $changestatus);
            autoexec::reset_task($taskclass);
            autoexec::schedule_task($taskclass);
        }
    }
}

echo $OUTPUT->header();

$tasks = autoexec::get_tasks();


$headerFields = array(
    get_string('taskname', 'local_autoexec'),
    get_string('taskclass', 'local_autoexec'),
    get_string('taskpath', 'local_autoexec'),
    get_string('taskfileexist', 'local_autoexec'),
    get_string('version', 'local_autoexec'),
    get_string('ticket', 'local_autoexec'),
    get_string('execute', 'local_autoexec'),
    get_string('delay', 'local_autoexec'),
    get_string('dependencies', 'local_autoexec'),
    get_string('executeruntime', 'local_autoexec'),
    get_string('executestarttime', 'local_autoexec'),
    get_string('executeendtime', 'local_autoexec'),
    get_string('executelogs', 'local_autoexec'),
    get_string('executestatus', 'local_autoexec')
);


echo '<table class="generaltable">';
echo generate_header($headerFields);
echo generate_body($tasks);
echo '</table>';


echo $OUTPUT->footer();


function generate_header($fields) {
    $i = 0;
    $htmlHeader = '<tr>';
    foreach($fields as $field) {
        $htmlHeader.= '<th class="header c'.$i.' ">'.$field.'</th>'."\n";
        $i++;
    }
    $htmlHeader .= '<th class="header c'.$i.' ">Adhoc</th>'."\n";
    $htmlHeader .= '<th class="header c'.($i+1).' ">Actions</th>'."\n";
    $htmlHeader .= '</tr>';
    return $htmlHeader;
}

function generate_body($tasks) {
    $htmlBody = '';
    foreach($tasks as $task) {
        $htmlBody .= generate_row($task);
    }
    return $htmlBody;
}

function generate_row($task) {
    $file_url = new moodle_url('/local/autoexec/manage_files.php', array('id'=> $task->id));
    $log_url = new moodle_url('/local/autoexec/manage_logs.php', array('id'=> $task->id));

    $htmlRow = '<tr>';
    $htmlRow .= '<td class="cell c0">'.$task->taskname.'</td>';
    $htmlRow .= '<td class="cell c1">'.$task->taskclass.'</td>';    
    $htmlRow .= '<td class="cell c2" style="color:'.(file_exists($task->taskpath)?'green':'red').'"><a href="'.$file_url->out().'" target="_blank">'.trim_taskpath($task->taskpath).'</a></td>';
    $htmlRow .= '<td class="centeralign cell c3">'.$task->taskfileexist.'</td>';
    $htmlRow .= '<td class="centeralign cell c4">'.$task->version.'</td>';
    $htmlRow .= '<td class="centeralign cell c5">'.$task->ticket.'</td>';
    $htmlRow .= '<td class="cell c6">'.get_execute_str($task->execute).'</td>';
    $htmlRow .= '<td class="cell c7">'.$task->delay.'</td>';
    $htmlRow .= '<td class="cell c8">'.$task->dependencies.'</td>';
    $htmlRow .= '<td class="cell c9">'.($task->executeruntime>0?time_to_date($task->executeruntime):'none').'</td>';
    $htmlRow .= '<td class="cell c10">'.($task->executestarttime>0?time_to_date($task->executestarttime):'none').'</td>';
    $htmlRow .= '<td class="cell c11">'.($task->executeendtime>0?time_to_date($task->executeendtime):'none').'</td>';
    $htmlRow .= '<td class="centeralign cell c12"><a href="'.$log_url->out().'" target="_blank"><i class="icon fa fa-clipboard"></i></a></td>';
    $htmlRow .= '<td class="centeralign cell c13">'.get_exec_status_str($task->executestatus).'</td>';
    $htmlRow .= '<td class="centeralign cell c14">'.(autoexec::get_planned_task($task->taskclass)!==false?'PLANNED':'NO').'</td>';
    $htmlRow .= '<td class="centeralign cell c15">';
    if ($task->executestatus != autoexec::EXEC_TODO) {
        $htmlRow .= '<form><input type="hidden" name="taskclass" value="'.$task->taskclass.'"/><input type="hidden" name="changestatus" value="'.autoexec::EXEC_TODO.'"/><input type="submit" value="TODO"/></form>';
    }
    if ($task->executestatus != autoexec::EXEC_MANUAL) {
        $htmlRow .= '<form><input type="hidden" name="taskclass" value="'.$task->taskclass.'"/><input type="hidden" name="changestatus" value="'.autoexec::EXEC_MANUAL.'"/><input type="submit" value="MANUAL"/></form>';
    }
    $htmlRow .= '</td>';
    $htmlRow .= '</tr>'."\n";
    return $htmlRow;
}

function time_to_date($time) {
    return date('Y-m-d H:i:s',$time);
}

function get_execute_str($status) {
    if ($status == 1) {
        return get_string('exec_auto', 'local_autoexec');
    }else{
        return get_string('exec_'.autoexec::EXEC_MANUAL, 'local_autoexec');
    }
    
}

function get_exec_status_str($status) {
    return get_string('exec_'.$status, 'local_autoexec');
}

function trim_taskpath($path) {
    $arr = explode('local/autoexec/tasks', $path, 2);
    if (count($arr) > 1) {
        return $arr[1];
    }
    return '';
}
