<?php

abstract class autoexec_task {
    
    function get_taskname() {
        return get_class($this);
    }
    
    function get_name() {
        return $this->get_config()['name'];
    }
    
    function get_config() {
        return array(
            'environnements' => array(), // ENV_PR/ENV_PP/ENV_INT/ENV_DEV, if empty run on all environnements
            'instances' => array(), // dgesco/ac-amiens, if empty run on all instances
            'version' => '0.0.0',
            'ticket' => '000', // ID of the associated ticket
            'execute' => true, // false to be executed manually
            'delay' => 0, // seconds to delay the execution
            'dependencies' => array(),
            'name' => 'Default Name'
        );
    }
    
    abstract function execute();
    
    function l($msg) {
        echo date('Y-m-d H:i:s__').$msg;
    }
    
}
