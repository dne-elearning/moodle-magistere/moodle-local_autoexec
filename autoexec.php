<?php

defined('MOODLE_INTERNAL') || die;

require_once($CFG->dirroot.'/local/autoexec/autoexec_task.php');

class autoexec {
    
    const TABLE_AUTOEXEC = 'local_autoexec';
    
    const TASK_PATH = '/tasks';
    
    const ENV_PR = 'ENV_PR';
    const ENV_PP = 'ENV_PP';
    const ENV_QP = 'ENV_QP';
    const ENV_INT = 'ENV_INT';
    const ENV_DEV = 'ENV_DEV';
    
    const EXEC_SUCCESS = 2;     // task succeded
    const EXEC_RUNNING = 3;     // task is currently executed
    const EXEC_FAIL = 4;        // task has failed, execute() return false
    const EXEC_ERROR = 5;       // task in error (exception)
    const EXEC_TODO = 6;        // task is waiting to be programmed
    const EXEC_QUEUED = 7;      // task is programmed and is waiting to be executed
    const EXEC_MANUAL = 8;      // task is waiting for a user action
    const EXEC_PROGRAMMING = 9; // task is currently being programmed
    
    const LOG_DISABLED = 2;
    const LOG_ENABLED = 3;
    
    static $logmode = self::LOG_DISABLED;
    static $logs = '';
    
    static function run_task($taskrecord) {
        global $DB;
        self::$logs = '';
        
        if ($taskrecord->executestatus != self::EXEC_QUEUED) {
            return false;
        }
        
        $DB->set_field(self::TABLE_AUTOEXEC, 'executestarttime', time(), array('taskclass'=>$taskrecord->taskclass));
        self::change_task_status($taskrecord->taskclass, self::EXEC_RUNNING);
        
        try {
            $task = self::get_task($taskrecord->taskclass);
            if ($task->execute()) {
                self::change_task_status($taskrecord->taskclass, self::EXEC_SUCCESS);
                
                //Search dependent task and try to plan them
                $dependents = $DB->get_records_sql('SELECT * FROM {'.self::TABLE_AUTOEXEC.'} WHERE dependencies LIKE ?', array('%'.$taskrecord->taskclass.'%'));
                if (count($dependents)>0){
                    foreach ($dependents AS $dependent) {
                        self::schedule_task($dependent);
                    }
                }
            }else{
                self::change_task_status($taskrecord->taskclass, self::EXEC_FAIL);
            }
        }catch(Exception $e){
            self::l('ERROR: '.$e->getMessage());
            self::l('STACK: '.$e->getTraceAsString());
            self::change_task_status($taskrecord->taskclass, self::EXEC_ERROR);
        }
        
        mtrace(self::$logs);
        
        self::save_logs($taskrecord->taskclass, self::$logs);
        $DB->set_field(self::TABLE_AUTOEXEC, 'executeendtime', time(), array('taskclass'=>$taskrecord->taskclass));
        
        
        
        return true;
    }
    
    static function save_logs($taskclass, $logs){
        $GLOBALS['DB']->set_field(self::TABLE_AUTOEXEC, 'executelogs', $logs, array('taskclass'=>$taskclass));
    }
    
    static function check_tasks() {
        global $DB;
        
        self::l('check_tasks()_BEGIN');
        $files = self::list_file_task();
        $tasks = $DB->get_records_sql('SELECT taskclass FROM {'.self::TABLE_AUTOEXEC.'}');
        
        self::l('check_tasks()_Checking new tasks');
        foreach($files AS $classname=>$file) {
            if (!array_key_exists($classname,$tasks)) {
                self::l('check_tasks()_New Task found "'.$classname.'"');
                // add the new task
                require_once($file);
                $obj = new $classname;
                $task_config = $obj->get_config();
                
                if (is_array($task_config['environnements']) && count($task_config['environnements']) > 0) {
                    self::l('check_tasks()_Environnement filtering enable for "'.$classname.'"');
                    if(!in_array(self::get_environnement(), $task_config['environnements'])) {
                        self::l('check_tasks()_Current environnement "'.self::get_environnement().'" not found "'.$classname.'"');
                        continue;
                    }else{
                        self::l('check_tasks()_Current environnement "'.self::get_environnement().'" found "'.$classname.'"');
                    }
                }else{
                    self::l('check_tasks()_Environnement filtering disabled for "'.$classname.'"');
                }
                
                if (is_array($task_config['instances']) && count($task_config['instances']) > 0) {
                    self::l('check_tasks()_Instance filtering enabled for "'.$classname.'"');
                    if(!in_array(self::get_instance(), $task_config['instances'])) {
                        self::l('check_tasks()_Current instance "'.self::get_instance().'" not found "'.$classname.'"');
                        continue;
                    }else{
                        self::l('check_tasks()_Current instance "'.self::get_instance().'" found "'.$classname.'"');
                    }
                }else{
                    self::l('check_tasks()_Instance filtering disabled for "'.$classname.'"');
                }
                
                $task = new stdClass();
                $task->taskname = (isset($task_config['name'])?$task_config['name']:$classname);
                $task->taskclass = $classname;
                $task->taskpath = $file;
                $task->taskfileexist = 1;
                $task->version = (isset($task_config['version'])?$task_config['version']:'0.0.0');
                $task->ticket = (isset($task_config['ticket'])?$task_config['ticket']:'0');
                $task->execute = (isset($task_config['execute'])?($task_config['execute']?1:0):0);
                $task->delay = (isset($task_config['delay'])?$task_config['delay']:0);
                $task->dependencies = (isset($task_config['dependencies'])?implode(',',$task_config['dependencies']):'');
                $task->executeruntime = 0;
                $task->executestarttime = 0;
                $task->executeendtime = 0;
                $task->executelogs = '';
                $task->executestatus = ($task->execute===0?self::EXEC_MANUAL:self::EXEC_TODO);
                
                $DB->insert_record(self::TABLE_AUTOEXEC, $task);
                self::l('check_tasks()_New task added "'.$classname.'"');
            }else{
                self::l('check_tasks()_Skipping existing Task "'.$classname.'"');
            }
        }
        
        self::l('check_tasks()_Program Task Execution');
        $tasks = $DB->get_records_sql('SELECT * FROM {'.self::TABLE_AUTOEXEC.'} WHERE executestatus = ?', array(self::EXEC_TODO));
        
        self::l('check_tasks()_'.count($tasks).' tasks to program');
        foreach($tasks AS $task) {
            self::schedule_task($task);
        }
        
        self::l('check_tasks()_END');
    }
    
    static function schedule_task($task) {
        global $DB;
        self::l('schedule_tack()_Programming task "'.$task->taskclass.'" ('.$task->id.')');
        
        $taskcheck = $DB->get_record(self::TABLE_AUTOEXEC, array('id'=>$task->id));
        if ($taskcheck === false){
            self::l('schedule_tack()_Task not found in the table');
            return false;
        }
        
        if ($taskcheck->executestatus != self::EXEC_TODO) {
            self::l('schedule_tack()_The task is not in TODO state (current status: '.$taskcheck->executestatus.')');
            return false;
        }
        
        self::l('schedule_tack()_Check dependencies "'.$taskcheck->dependencies.'"');
        if (!empty($taskcheck->dependencies)) {
            $dependencies = explode(',', $taskcheck->dependencies);
            self::l('schedule_tack()_Found "'.count($dependencies).'" dependencies');
            foreach ($dependencies AS $dependence) {
                self::l('schedule_tack()_Check dependence "'.$dependence.'"');
                $checkdep = $DB->get_record(self::TABLE_AUTOEXEC, array('taskclass'=>$dependence));
                if ($checkdep === false || $checkdep->executestatus != self::EXEC_SUCCESS) {
                    self::l('schedule_tack()_Dependence is not completed');
                    return false;
                }
                self::l('schedule_tack()_Dependence is validated');
            }
        }else{
            self::l('schedule_tack()_No dependencies for this task');
        }
        
        self::l('schedule_tack()_Check adhoc task');
        $adhoctask = self::get_planned_task($taskcheck->taskclass);
        if ($adhoctask !== false){
            self::l('schedule_tack()_Found adhoc task, we skip');
            return true;
        }
        
        self::l('schedule_tack()_Create adhoc task');
        self::change_task_status($taskcheck->taskclass, self::EXEC_PROGRAMMING);
        $tsk = self::get_execute_tack();
        $tsk->set_custom_data(array('taskclass'=>$taskcheck->taskclass));
        $tsk->set_next_run_time(time()+$taskcheck->delay);
        \core\task\manager::queue_adhoc_task($tsk);
        
        $GLOBALS['DB']->set_field(self::TABLE_AUTOEXEC, 'executeruntime', time()+$taskcheck->delay, array('taskclass'=>$task->taskclass));
        
        self::change_task_status($taskcheck->taskclass, self::EXEC_QUEUED);
        
        self::l('schedule_tack()_Programming task END');
    }
    
    
    static function reset_task($taskclass) {
        global $DB;
        $task = $DB->get_record(self::TABLE_AUTOEXEC, array('taskclass'=>$taskclass), 'id, taskclass, executestatus, delay');
        
        if ($task->executestatus != self::EXEC_TODO) {
            return false;
        }
        
        $task->executeruntime = time()+$task->delay;
        $task->executestarttime = 0;
        $task->executeendtime = 0;
        $task->executelogs = '';
        $DB->update_record(self::TABLE_AUTOEXEC, $task);
    }
    
    static function get_planned_task($taskclass) {
        return $GLOBALS['DB']->get_record_sql('SELECT * FROM {task_adhoc} WHERE classname = :classname AND customdata = :customdata', array('classname'=>'_local_autoexec_task_execute_task', 'customdata' => '{"taskclass":"'.$taskclass.'"}'));
    }
    
    static function get_execute_tack() {
        return new \local_autoexec\task\execute_task;
    }
    
    static function change_task_status($taskclass, $status) {
        $GLOBALS['DB']->set_field(self::TABLE_AUTOEXEC, 'executestatus', $status, array('taskclass'=>$taskclass));
    }
    
    static function get_instance() {
        return $GLOBALS['CFG']->academie_name;
    }
    
    static function get_environnement() {
        global $CFG;
        if ($CFG->magistere_domaine == 'https://magistere.education.fr') {
            return self::ENV_PR;
        } else if ($CFG->magistere_domaine == 'https://pp-magistere.foad.hp.in.phm.education.gouv.fr') {
            return self::ENV_PP;
        } else if ($CFG->magistere_domaine == 'https://qp-magistere.foad.hp.in.phm.education.gouv.fr') {
            return self::ENV_QP;
        } else if ($CFG->magistere_domaine == 'https://int.magistere.fr') {
            return self::ENV_INT;
        } else {
            return self::ENV_DEV;
        }
    }
    
    static function list_file_task() {
        global $CFG;
        $taskpath = $CFG->dirroot.'/local/autoexec'.self::TASK_PATH;
        $files = self::getDirContents($taskpath);
        $tasks = array();
        foreach ($files AS $file) {
            if (strpos($file,'.php') == strlen($file)-4) {
                require_once($file);
                $filename = substr($file,strrpos($file,'/')+1);
                $classname = substr($filename, 0, strrpos($filename,'.php'));
                $tasks[$classname] = $file;
            }
        }
        return $tasks;
    }
    
    private static function getDirContents($dir, &$results = array()) {
        $files = scandir($dir);
        foreach ($files as $value) {
            $path = realpath($dir . DIRECTORY_SEPARATOR . $value);
            if (!is_dir($path)) {
                $results[] = $path;
            } else if ($value != "." && $value != "..") {
                self::getDirContents($path, $results);
            }
        }
        return $results;
    }
    
    public static function get_task($taskclass) {
        global $DB;
        
        $taskrecord = $DB->get_record('local_autoexec', array('taskclass'=>$taskclass));
        
        if ($taskrecord === false) {
            return false;
        }
        
        if (!file_exists($taskrecord->taskpath)) {
            return false;
        }
        
        require_once($taskrecord->taskpath);
        if (!class_exists($taskrecord->taskclass)) {
            return false;
        }
        return new $taskrecord->taskclass;
    }
    
    public static function get_tasks() {
        global $DB;
        
        $taskrecords = $DB->get_records('local_autoexec', null, 'id DESC');
        return $taskrecords;
    }
    
    public static function l($msg) {
        if (self::$logmode == self::LOG_ENABLED){
            echo date('Y-m-d H:i:s__').$msg."\n";
        }
        self::$logs .= date('Y-m-d H:i:s__').$msg."\n";
    }
    
}
