<?php 

require_once($CFG->libdir.'/externallib.php');
require_once($CFG->dirroot.'/local/autoexec/autoexec.php');


class local_autoexec_external extends external_api {
    
    public const DATE_TIME_FORMAT = 'Y-m-d H:i:s';
    
    private static $so = '';
    private static $soasc = true;
    
    public static function update_task_parameters(){
        return new external_function_parameters(
            array(
                'action' => new external_value(PARAM_RAW, ''),
                'instance' => new external_value(PARAM_RAW, ''),
                'id' => new external_value(PARAM_INT, '')
            )
        );
    }
    
    public static function update_task_returns(){
        return new external_single_structure(
            array(
                'data' => new external_value(PARAM_RAW, '')
            )
        );
    }
    
    public static function update_task( $action, $instance, $id ){
        
        global $CFG;
        require_once($CFG->dirroot.'/local/magisterelib/databaseConnection.php');
        
        $data = new stdClass();
        
        if ($action == 'manual'){
            
        }else if ($action == 'todo'){
            
            $magistere_academies = get_magistere_academy_config();
            
            if (array_key_exists($instance, $magistere_academies)){
                
                if ((databaseConnection::instance()->get($instance)) === false){
                    error_log('externallib.php/update_task/'.$instance.'/Database_connection_failed');
                    $data->error = true;
                    $data->msg = 'instance error';
                    return array('data'=>json_encode($data));
                }
                
                $autotask = databaseConnection::instance()->get($instance)->get_record(autoexec::TABLE_AUTOEXEC, array('id'=>$id));
                
                $autotask->execute = autoexec::EXEC_TODO;
                $autotask->executeruntime = time();
                $autotask->executestarttime = 0;
                $autotask->executeendtime = 0;
                $autotask->executelogs = '';
                $autotask->executestatus = autoexec::EXEC_TODO;
                
                databaseConnection::instance()->get($instance)->update_record(autoexec::TABLE_AUTOEXEC, $autotask);
                
                $jdata = new stdClass();
                $jdata->id = $id;
                
                $task = new stdClass();
                $task->component = 'local_autoexec';
                $task->classname = '\local_autoexec\task\schedule_task';
                $task->nextruntime = time();
                $task->faildelay = 0;
                $task->customdata = json_encode($jdata);
                $task->userid = 2;
                $task->blocking = 0;
                
                if (databaseConnection::instance()->get($instance)->insert_record('task_adhoc', $task)){
                    $data->error = false;
                }else{
                    $data->error = true;
                    $data->msg = 'scheduling failed';
                }
                
            }else{
                $data->error = true;
                $data->msg = 'invalid instance';
            }
            
        }else{
            $data->error = true;
            $data->msg = 'invalid action';
        }
        
        return array('data'=>json_encode($data));
    }
    
    public static function get_alltasks_parameters(){
        return new external_function_parameters(
            array(
                /*
                'tasks'  => new external_multiple_structure(new external_value(PARAM_RAW, '')),
                'instances'  => new external_multiple_structure(new external_value(PARAM_RAW, '')),
                'laststatus' => new external_multiple_structure(new external_value(PARAM_RAW, '')),
                */
                'si' => new external_value(PARAM_INT, ''),
                'ps' => new external_value(PARAM_INT, ''),
                'so' => new external_value(PARAM_RAW, '')
            )
        );
        
    }
    
    public static function get_alltasks_returns(){
        return new external_single_structure(
            array(
                'data' => new external_value(PARAM_RAW, '')
            )
        );
    }
    
    public static function get_alltasks(  $si, $ps, $so ){ //$filter_tasks, $filter_instances, $filter_laststatus,
        
        global $CFG;
        require_once($CFG->dirroot.'/local/magisterelib/databaseConnection.php');
        /*
        $where = '';
        $params = array();
        if (count($filter_tasks) > 0){
            $filterwhere = array();
            $filterparams = array();
            $i = 0;
            foreach($filter_tasks AS $filter_task){
                $filterwhere[] = ':ftask'.$i;
                $filterparams[':ftask'.$i] = $filter_task;
                $i++;
            }
            $where .= ' AND taskclass IN ('.implode(' OR ',$filterwhere).')';
            $params = array_merge($params,$filterparams);
        }
        
        if (count($filter_laststatus) > 0){
            $filterwhere = array();
            $filterparams = array();
            $i = 0;
            foreach($filter_tasks AS $filter_task){
                $filterwhere[] = ':ftask'.$i;
                $filterparams[':ftask'.$i] = $filter_task;
                $i++;
            }
            $where .= ' AND ('.implode(' OR ',$filterwhere).')';
            $params = array_merge($params,$filterparams);
        }
        */
        
        $tasks = array();
        $magistere_academies = get_magistere_academy_config();
        foreach ($magistere_academies as $academy_name => $data)
        {
            if($academy_name == 'frontal'){continue;}
            //if (count($filter_instances) > 0 && !in_array($academy_name, $filter_instances)){continue;}
            if ((databaseConnection::instance()->get($academy_name)) === false){error_log('autoexec.php/get_courses_list/'.$academy_name.'/Database_connection_failed'); continue;}
            
            $tasksraw = databaseConnection::instance()->get($academy_name)->get_records_sql("SELECT * FROM {".autoexec::TABLE_AUTOEXEC."}");
        
            foreach($tasksraw AS $taskraw) {
                
                $file_url = new moodle_url('/local/autoexec/manage_files.php', array('id'=> $taskraw->id));
                $log_url = new moodle_url('/local/autoexec/manageall_logs.php', array('id'=> $taskraw->id,'instance'=>$academy_name));
                
                $taskraw->instance = $academy_name;
                $taskraw->taskfileexist =  $taskraw->taskfileexist == 1 ? get_string('exist', 'local_autoexec') : get_string('noexist', 'local_autoexec');
                $taskraw->execute = $taskraw->execute == 1 ? get_string('exec_auto', 'local_autoexec') : get_string('exec_manual', 'local_autoexec');
                
                $taskraw->executelogs      = '<a href="'.$log_url->out().'" target="_blank"><i class="icon fa fa-clipboard"></i></a>';
                $taskraw->taskpath         = '<a href="'.$file_url->out().'" target="_blank">'.self::trim_taskpath($taskraw->taskpath).'</a>';
                $taskraw->dependencies     = str_replace(',',",<br/>",$taskraw->dependencies);
                
                $taskraw->executeruntime   = $taskraw->executeruntime>0?date(self::DATE_TIME_FORMAT, $taskraw->executeruntime):'none';
                $taskraw->executestarttime = $taskraw->executestarttime>0?date(self::DATE_TIME_FORMAT, $taskraw->executestarttime):'none';
                $taskraw->executeendtime   = $taskraw->executeendtime>0?date(self::DATE_TIME_FORMAT, $taskraw->executeendtime):'none';
                $taskraw->executestatus    = get_string('exec_'.$taskraw->executestatus, 'local_autoexec');
                
                $taskraw->actions = '<button id="todo_'.$taskraw->instance.'_'.$taskraw->id.'" class="todobutton">Todo</button>
<button id="manual_'.$taskraw->instance.'_'.$taskraw->id.'" class="manualbutton">Manual</button>';
                
                $tasks[] = $taskraw;
            }
        }
        
        $so_array = explode(' ',$so);
        self::$so = $so_array[0];
        self::$soasc = $so_array[1] != 'DESC';
        usort($tasks, array(self::class,'tasksort'));
        
        $tasks_limit = array_slice($tasks, $si, $ps);

        $jTableResult = array();
        $jTableResult['Result'] = "OK";
        $jTableResult['TotalRecordCount'] = count($tasks);
        $jTableResult['Records'] = $tasks_limit;
        
        return array('data'=>json_encode($jTableResult));

    }
    
    static function tasksort($a, $b){
        
        if ($a->{self::$so} == $b->{self::$so}){
            return 0;
        }
        if (self::$soasc){
            return $a->{self::$so} < $b->{self::$so}?-1:1;
        }else{
            return $a->{self::$so} > $b->{self::$so}?-1:1;
        }
    }
    
    static function time_to_date($time) {
        return date('Y-m-d H:i:s',$time);
    }
    
    static function get_execute_str($status) {
        if ($status == 1) {
            return get_string('exec_auto', 'local_autoexec');
        }else{
            return get_string('exec_'.autoexec::EXEC_MANUAL, 'local_autoexec');
        }
        
    }
    
    static function get_exec_status_str($status) {
        return get_string('exec_'.$status, 'local_autoexec');
    }
    
    static function trim_taskpath($path) {
        $arr = explode('local/autoexec/tasks', $path, 2);
        if (count($arr) > 1) {
            return $arr[1];
        }
        return '';
    }
    
}