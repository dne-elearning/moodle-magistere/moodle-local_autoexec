<?php

class task_005_auto_task_dep01 extends autoexec_task {
    
    
    
    function get_config() {
        return array(
            'environnements' => array(), // PR/PP/INT/DEV
            'instances' => array(), // dgesco/ac-amiens
            'version' => '0.0.0',
            'ticket' => '005', // ID of the associated ticket
            'execute' => true, // false to be executed manually
            'delay' => 0, // seconds to delay the execution
            'name' => 'task_005_auto_task_dep01'
        );
    }
    
    
    
    
    
    function execute() {
        autoexec::l('START demo script task_005_auto_task_dep01');
        autoexec::l(print_r($this->get_config(),true));
        autoexec::l('END demo script task_005_auto_task_dep01');
        return true;
    }
    
    
    
    
    
    
    
}