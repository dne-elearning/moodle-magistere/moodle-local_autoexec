<?php

class task_003_auto_task extends autoexec_task {
    
    
    
    function get_config() {
        return array(
            'environnements' => array(), // PR/PP/INT/DEV
            'instances' => array(), // dgesco/ac-amiens
            'version' => '0.0.0',
            'ticket' => '003', // ID of the associated ticket
            'execute' => true, // false to be executed manually
            'delay' => 0, // seconds to delay the execution
            'name' => 'task_003_auto_task'
        );
    }
    
    
    
    
    
    function execute() {
        autoexec::l('START demo script task_003_auto_task');
        autoexec::l(print_r($this->get_config(),true));
        autoexec::l('END demo script task_003_auto_task');
        return true;
    }
    
    
    
    
    
    
    
}