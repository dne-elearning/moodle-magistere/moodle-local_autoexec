<?php

class task_008_auto_task_dep04_01_02_03 extends autoexec_task {
    
    
    
    function get_config() {
        return array(
            'environnements' => array(), // PR/PP/INT/DEV
            'instances' => array(), // dgesco/ac-amiens
            'version' => '0.0.0',
            'ticket' => '008', // ID of the associated ticket
            'execute' => true, // false to be executed manually
            'delay' => 0, // seconds to delay the execution
            'name' => 'task_007_auto_task_dep03_01',
            'dependencies' => array('task_005_auto_task_dep01','task_006_auto_task_dep02_01','task_007_auto_task_dep03_01')
        );
    }
    
    
    
    
    
    function execute() {
        autoexec::l('START demo script task_008_auto_task_dep04_01_02_03');
        autoexec::l(print_r($this->get_config(),true));
        autoexec::l('END demo script task_008_auto_task_dep04_01_02_03');
        return true;
    }
    
    
    
    
    
    
    
}