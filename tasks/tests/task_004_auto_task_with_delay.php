<?php

class task_004_auto_task_with_delay extends autoexec_task {
    
    
    
    function get_config() {
        return array(
            'environnements' => array(), // PR/PP/INT/DEV
            'instances' => array(), // dgesco/ac-amiens
            'version' => '0.0.0',
            'ticket' => '004', // ID of the associated ticket
            'execute' => true, // false to be executed manually
            'delay' => 120, // seconds to delay the execution
            'name' => 'task_004_auto_task_with_delay'
        );
    }
    
    
    
    
    
    function execute() {
        autoexec::l('START demo script task_004_auto_task_with_delay');
        autoexec::l(print_r($this->get_config(),true));
        autoexec::l('END demo script task_004_auto_task_with_delay');
        return true;
    }
    
    
    
    
    
    
    
}