<?php

require_once(dirname(dirname(dirname(__FILE__))).'/config.php');
require_once($CFG->dirroot.'/local/autoexec/autoexec.php');

require_login();

if ($USER->id != 2) {
    print_error('Acces denied');
}

$PAGE->requires->jquery();
$PAGE->requires->jquery_plugin('ui');
$PAGE->requires->jquery_plugin('ui-css');

$PAGE->set_pagelayout('frontal_notconnected');
$PAGE->set_context(context_system::instance());

$pageparams = array();
$PAGE->set_url('/local/autoexec/manageAll.php', $pageparams);

$taskclass = optional_param('taskclass', false, PARAM_ALPHANUMEXT);
$changestatus = optional_param('changestatus', false, PARAM_INT);

/*
if ($taskclass !== false && $changestatus !== false) {
    autoexec::change_task_status($taskclass, $status);
    autoexec::reset_task($taskclass);
    autoexec::schedule_task($taskclass);
}*/


if ($taskclass !== false && $changestatus !== false) {
    
    $task = $DB->get_record(autoexec::TABLE_AUTOEXEC, array('taskclass'=>$taskclass));
    
    if ($task !== false) {
        if ($changestatus == autoexec::EXEC_TODO) {
            autoexec::change_task_status($taskclass, $changestatus);
            autoexec::reset_task($taskclass);
            autoexec::schedule_task($taskclass);
        } else if ($changestatus == autoexec::EXEC_MANUAL) {
            autoexec::change_task_status($taskclass, $changestatus);
            autoexec::reset_task($taskclass);
            autoexec::schedule_task($taskclass);
        }
    }
}

echo $OUTPUT->header();

// Load strings
$strs = new stdClass();
$strs->manageall_header_instance        = get_string('instance', 'local_autoexec');
$strs->manageall_header_taskname        = get_string('taskname', 'local_autoexec');
$strs->manageall_header_taskclass       = get_string('taskclass', 'local_autoexec');
$strs->manageall_header_taskpath        = get_string('taskpath', 'local_autoexec');
$strs->manageall_header_taskfileexist   = get_string('taskfileexist', 'local_autoexec');
$strs->manageall_header_version         = get_string('version', 'local_autoexec');
$strs->manageall_header_ticket          = get_string('ticket', 'local_autoexec');
$strs->manageall_header_execute         = get_string('execute', 'local_autoexec');
$strs->manageall_header_delay           = get_string('delay', 'local_autoexec');
$strs->manageall_header_dependencies    = get_string('dependencies', 'local_autoexec');
$strs->manageall_header_executeruntime  = get_string('executeruntime', 'local_autoexec');
$strs->manageall_header_executestarttime= get_string('executestarttime', 'local_autoexec');
$strs->manageall_header_executeendtime  = get_string('executeendtime', 'local_autoexec');
$strs->manageall_header_executelogs     = get_string('executelogs', 'local_autoexec');
$strs->manageall_header_executestatus   = get_string('executestatus', 'local_autoexec');


// Call amd file
$PAGE->requires->js_call_amd('local_autoexec/form', 'init', array('str'=>$strs));

echo '<div id="managetable"></div>';
echo $OUTPUT->footer();
