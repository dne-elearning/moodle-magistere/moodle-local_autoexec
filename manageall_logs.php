<?php

require_once(dirname(dirname(dirname(__FILE__))).'/config.php');
require_once($CFG->dirroot.'/local/autoexec/autoexec.php');
require_once($CFG->dirroot.'/local/magisterelib/databaseConnection.php');

require_login();

if ($USER->id != 2) {
    print_error('Acces denied');
}

$id = required_param('id', PARAM_INT);
$instance = required_param('instance', PARAM_ALPHAEXT);

if (databaseConnection::instance()->get($instance) === false){
    print_error('DB connection failed');
}

$record = databaseConnection::instance()->get($instance)->get_record(autoexec::TABLE_AUTOEXEC, array('id'=>$id), 'id, taskclass, executelogs');

if ($record === false){
    die('ERROR: Task not found');
}

echo '<h1>Task log "'.$record->taskclass.'"</h1>';
echo '<pre style="background-color:#CCCCCC">';
echo $record->executelogs;
echo '</pre>';