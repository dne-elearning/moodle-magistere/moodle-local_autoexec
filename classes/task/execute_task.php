<?php

/**
 * Adhoc task that processes autoexec tasks
 *
 * @package    local_autoexec
 * @copyright  2021 TCS
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

namespace local_autoexec\task;

defined('MOODLE_INTERNAL') || die();

require_once($CFG->dirroot.'/local/autoexec/autoexec.php');

/**
 * 
 * @package     local_autoexec
 * @copyright   2021 TCS
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class execute_task extends \core\task\adhoc_task {
    
    public function get_name() {
        return 'Execute tasks';
        //return get_string('sendnotification', 'mod_bigbluebuttonbn');
    }

    public function execute() {
        global $DB;

        $data = $this->get_custom_data();
        mtrace('Running task "'.$data->taskclass.'"');
        
        $taskcheck = $DB->get_record('local_autoexec', array('taskclass'=>$data->taskclass));
        if ($taskcheck === false){
            \autoexec::l('schedule_tack()_Task not found in the table');
            return false;
        }
        
        if ($taskcheck->executestatus == \autoexec::EXEC_QUEUED) {
            \autoexec::run_task($taskcheck);
        }
        
        mtrace('Task completed "'.$data->taskclass.'"');
    }
}
