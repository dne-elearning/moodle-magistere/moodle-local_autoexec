<?php

namespace local_autoexec\task;

require_once($CFG->dirroot.'/local/autoexec/autoexec.php');

class check_upgrade_task extends \core\task\scheduled_task
{
    public function get_name()
    {
        return "Check upgrade Task";
    }

    public function execute() 
    {
        \autoexec::$logmode = \autoexec::LOG_ENABLED;
    	\autoexec::check_tasks();
    }
}