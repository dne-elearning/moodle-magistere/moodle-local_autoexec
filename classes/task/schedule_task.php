<?php

/**
 * Adhoc task that schedule autoexec tasks
 *
 * @package    local_autoexec
 * @copyright  2021 TCS
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

namespace local_autoexec\task;

defined('MOODLE_INTERNAL') || die();

require_once($CFG->dirroot.'/local/autoexec/autoexec.php');

/**
 * 
 * @package     local_autoexec
 * @copyright   2021 TCS
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class schedule_task extends \core\task\adhoc_task {
    
    public function get_name() {
        return 'Schedule tasks';
        //return get_string('sendnotification', 'mod_bigbluebuttonbn');
    }

    public function execute() {
        global $DB;
        
        \autoexec::$logmode = \autoexec::LOG_ENABLED;

        $data = $this->get_custom_data();
        mtrace('Scheduling task "'.$data->id.'"');
        
        $task = $DB->get_record('local_autoexec', array('id'=>$data->id));
        if ($task === false){
            \autoexec::l('schedule_tack()_Task not found in the table');
            return false;
        }
        
        \autoexec::schedule_task($task);
        
        mtrace('Task scheduled "'.$data->id.'"');
    }
}
